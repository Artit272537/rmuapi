package com.rmu.it.rest;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rmu.it.dto.StudentDto;
import com.rmu.it.service.LineNotifyService;
import com.rmu.it.util.ResponeDto;

@RestController
@RequestMapping("/notify")
public class LineNotifyController {

	private static final Logger log = LoggerFactory.getLogger(LineNotifyController.class);
	
	@Autowired(required = false)
	LineNotifyService lineNotifyService;

	@GetMapping(path = "/sendLineNotify")
	public ResponseEntity sendLineNotify(@RequestParam("message") String message) throws Exception {
		log.info("sendLineNotify : Start");
		ResponeDto<String> respone = new ResponeDto<>();
		try {
			if (lineNotifyService.sendLineNotify(message)) {
				respone.setResponseCode(ResponeDto.RESPONSE_CODE.SUCCESS01.getCode());
				respone.setResponseMessage(ResponeDto.RESPONSE_CODE.SUCCESS01.getValue());
				respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.SUCCESS.getCode());
			} else {
				respone.setResponseCode(ResponeDto.RESPONSE_CODE.SUCCESS02.getCode());
				respone.setResponseMessage(ResponeDto.RESPONSE_CODE.SUCCESS02.getValue());
				respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.SUCCESS.getCode());
			}

		} catch (Exception e) {
			log.error("sendLineNotify Fail" + e.getMessage(), e);
			respone.setResponseCode(ResponeDto.RESPONSE_CODE.FAIL01.getCode());
			respone.setResponseMessage(ResponeDto.RESPONSE_CODE.FAIL01.getValue());
			respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.SUCCESS.getCode());
		}

		log.info("sendLineNotify : End");
		return new ResponseEntity<ResponeDto<String>>(respone, HttpStatus.OK);
	}
}
