package com.rmu.it.rest;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rmu.it.dto.StudentDto;
import com.rmu.it.service.StudentService;
import com.rmu.it.util.ResponeDto;

@RestController
@RequestMapping("/api")
public class RMUApiController {
	private static final Logger log = LoggerFactory.getLogger(RMUApiController.class);
	
	@Autowired(required = false)
	StudentService studentService;
	
	@GetMapping(path = "/student/{citizen}")
	public ResponseEntity getStudentByCitizen(@PathVariable("citizen") String citizen) throws Exception {
		log.info("getStudentByCitizen : Start");
		log.info("citizen => "+citizen);
		ResponeDto<StudentDto> respone = new ResponeDto<>();
		try {
			StudentDto student = studentService.getStudentByCitizen(citizen);
			List<StudentDto> students = Arrays.asList(student);
			respone.setData(students);
			if (!respone.getData().isEmpty()) {
				respone.setResponseCode(ResponeDto.RESPONSE_CODE.SUCCESS01.getCode());
				respone.setResponseMessage(ResponeDto.RESPONSE_CODE.SUCCESS01.getValue());
				respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.SUCCESS.getCode());
			} else {
				respone.setResponseCode(ResponeDto.RESPONSE_CODE.SUCCESS02.getCode());
				respone.setResponseMessage(ResponeDto.RESPONSE_CODE.SUCCESS02.getValue());
				respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.SUCCESS.getCode());
			}

		} catch (Exception e) {
			log.error("getStudentByCitizen Fail" + e.getMessage(), e);
			respone.setResponseCode(ResponeDto.RESPONSE_CODE.FAIL01.getCode());
			respone.setResponseMessage(ResponeDto.RESPONSE_CODE.FAIL01.getValue());
			respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.SUCCESS.getCode());
		}

		log.info("getStudentByCitizen : End");
		return new ResponseEntity<ResponeDto<StudentDto>>(respone, HttpStatus.OK);
	}
	
	@PostMapping(path = "/save")
	public ResponseEntity saveStudent(@RequestBody StudentDto dto) throws Exception {
		log.info("saveStudent : Start");
		log.info("StudentDto => "+dto.toString());
		ResponeDto<StudentDto> respone = new ResponeDto<>();
		try {
			StudentDto resp = studentService.saveStudent(dto);
			List<StudentDto> list = Arrays.asList(resp);
			respone.setData(list);
			if (!respone.getData().isEmpty() && respone.getData() != null) {
				respone.setResponseCode(ResponeDto.RESPONSE_CODE.SUCCESS01.getCode());
				respone.setResponseMessage(ResponeDto.RESPONSE_CODE.SUCCESS01.getValue());
				respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.SUCCESS.getCode());
			} else {
				respone.setResponseCode(ResponeDto.RESPONSE_CODE.FAIL01.getCode());
				respone.setResponseMessage(ResponeDto.RESPONSE_CODE.FAIL01.getValue());
				respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.FAIL.getCode());
			}

		} catch (Exception e) {
			log.error("saveStudent Fail" + e.getMessage(), e);
			respone.setResponseCode(ResponeDto.RESPONSE_CODE.FAIL01.getCode());
			respone.setResponseMessage(ResponeDto.RESPONSE_CODE.FAIL01.getValue());
			respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.FAIL.getCode());
		}

		log.info("saveStudent : End");
		return new ResponseEntity<ResponeDto<StudentDto>>(respone, HttpStatus.OK);
	}
	
	
	@DeleteMapping(path = "/delete/{sId}")
	public ResponseEntity deleteStudent(@PathVariable("sId") Long sId) throws Exception {
		log.info("deleteStudent : Start");
		log.info("sId => "+sId);
		ResponeDto<StudentDto> respone = new ResponeDto<>();
		try {
			boolean deleteStatus = studentService.deleteStudent(sId);
			if (deleteStatus) {
				respone.setResponseCode(ResponeDto.RESPONSE_CODE.SUCCESS01.getCode());
				respone.setResponseMessage(ResponeDto.RESPONSE_CODE.SUCCESS01.getValue());
				respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.SUCCESS.getCode());
			} else {
				respone.setResponseCode(ResponeDto.RESPONSE_CODE.FAIL01.getCode());
				respone.setResponseMessage(ResponeDto.RESPONSE_CODE.FAIL01.getValue());
				respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.FAIL.getCode());
			}

		} catch (Exception e) {
			log.error("saveStudent Fail" + e.getMessage(), e);
			respone.setResponseCode(ResponeDto.RESPONSE_CODE.FAIL01.getCode());
			respone.setResponseMessage(ResponeDto.RESPONSE_CODE.FAIL01.getValue());
			respone.setResponseStatus(ResponeDto.RESPONSE_STATUS.FAIL.getCode());
		}

		log.info("saveStudent : End");
		return new ResponseEntity<ResponeDto<StudentDto>>(respone, HttpStatus.OK);
	}
	
	
}
