package com.rmu.it.rest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rmu.it.service.ReportService;
import com.rmu.it.util.ServiceUtils;

@RestController
@RequestMapping("/report")
public class ReportController {
	private static final Logger log = LoggerFactory.getLogger(ReportController.class);

	@Autowired(required = false)
	ReportService reportService;
	
	@GetMapping(path = "/generateReport")
	public ResponseEntity<InputStreamResource> generateReport(HttpServletRequest request) throws IOException{
		log.info("generateReport : Start");
		ResponseEntity<InputStreamResource> response = null;
		try (ByteArrayOutputStream out = reportService.generateReport()){
			if(out != null) {
				response = new  ResponseEntity<>(new InputStreamResource(new ByteArrayInputStream(out.toByteArray())),
						ServiceUtils.createResponseHeader(MediaType.APPLICATION_PDF, "test.pdf", null),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			log.error("generateReport Error =>"+e.getMessage() , e);
		}
		log.info("generateReport : End");
		return response;
	}
	
}
