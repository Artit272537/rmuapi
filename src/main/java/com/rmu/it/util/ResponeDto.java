package com.rmu.it.util;

import java.util.List;

public class ResponeDto<T> {

	private List<T> data;

	private String responseCode;

	private String responseMessage;

	private String responseStatus;

	public enum RESPONSE_CODE {
		SUCCESS01("SUCCESS-01", "Success"), SUCCESS02("SUCCESS-02", "Success With No Data"),
		FAIL01("F01-FAILED", "Internal error");

		private String code;
		private String value;

		private RESPONSE_CODE(String code, String value) {
			this.code = code;
			this.value = value;
		}

		public String getCode() {
			return code;
		}

		public String getValue() {
			return value;
		}

	}

	public enum RESPONSE_STATUS {
		SUCCESS("S", "Success"), FAIL("F", "Fail");

		private String code;
		private String value;

		private RESPONSE_STATUS(String code, String value) {
			this.code = code;
			this.value = value;
		}

		public String getCode() {
			return code;
		}

		public String getValue() {
			return value;
		}

	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

}
