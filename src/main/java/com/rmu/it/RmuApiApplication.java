package com.rmu.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.rmu.it.*"})
public class RmuApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RmuApiApplication.class, args);
	}
}
