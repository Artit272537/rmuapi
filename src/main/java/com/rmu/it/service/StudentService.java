package com.rmu.it.service;

import com.rmu.it.dto.StudentDto;

public interface StudentService {
	
	public StudentDto getStudentByCitizen(String citizen) throws Exception;

	public StudentDto saveStudent(StudentDto dto) throws Exception;

	public boolean deleteStudent(Long sId) throws Exception;

}
