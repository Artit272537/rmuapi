package com.rmu.it.service;

public interface LineNotifyService {
	boolean sendLineNotify(String message) throws Exception;
}
