package com.rmu.it.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rmu.it.dao.StudentRepository;
import com.rmu.it.dto.StudentDto;
import com.rmu.it.entity.StudentEntity;
import com.rmu.it.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{

	private static final Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);
			
	@Autowired(required = false)
	StudentRepository studentRepository;
	
	@Override
	public StudentDto getStudentByCitizen(String citizen) throws Exception {
		log.info("getStudentByCitizen : Start");
		StudentEntity entity = new StudentEntity();
		try {
			 entity = studentRepository.findStudentByCitizen(citizen);
		} catch (Exception e) {
			log.error("getStudentByCitizen Error => "+e.getMessage());
		}
		log.info("getStudentByCitizen : End");
		return this.convEntityToDto(entity);
	}
	
	private StudentDto convEntityToDto(StudentEntity entity) {
		StudentDto dto = new StudentDto();
		if(entity != null) {
			dto.setsId(entity.getsId());
			dto.setCitizen(entity.getCitizen());
			dto.setPrefix(entity.getPrefix());
			dto.setName(entity.getName());
			dto.setLname(entity.getLname());
			dto.setNickName(entity.getNickName());
			dto.setGender(entity.getGender());
			dto.setPhone(entity.getPhone());
			dto.setEmail(entity.getEmail());
			dto.setFacebook(entity.getFacebook());
			dto.setLine(entity.getLine());
		}
		return dto;
	}
	
	private StudentEntity convDtoToEntity(StudentDto dto) {
		StudentEntity entity = new StudentEntity();
		if(dto != null) {
			entity.setsId(dto.getsId());
			entity.setCitizen(dto.getCitizen());
			entity.setPrefix(dto.getPrefix());
			entity.setName(dto.getName());
			entity.setLname(dto.getLname());
			entity.setNickName(dto.getNickName());
			entity.setGender(dto.getGender());
			entity.setPhone(dto.getPhone());
			entity.setEmail(dto.getEmail());
			entity.setFacebook(dto.getFacebook());
			entity.setLine(dto.getLine());
		}
		return entity;
	}

	@Override
	public StudentDto saveStudent(StudentDto dto) throws Exception {
		
		StudentEntity entity = this.convDtoToEntity(dto);
		try {
			if(entity != null) {
			 studentRepository.save(entity);
			}
		} catch (Exception e) {
			log.error("saveStudent => "+e.getMessage());
		}	
		return this.getStudentByCitizen(entity.getCitizen());
	}

	@Override
	public boolean deleteStudent(Long sId) throws Exception {
		boolean result = false;
		try {
			if(sId !=null)
				studentRepository.deleteById(sId);
			result = true;
		} catch (Exception e) {
			log.error("deleteStudent Error => "+e.getMessage());
		}
		return result;
	}

}
