package com.rmu.it.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {

	public static void main(String[] args) throws ParseException {
		String dateStr = "Mon Nov 12 2018 00:00:00 GMT 0700";
		SimpleDateFormat fm  = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss z");
		SimpleDateFormat fm2  = new SimpleDateFormat("ddMMyyyy");
		Date date = fm.parse(dateStr);
		String d = fm2.format(date);
		System.out.println(date);
		System.out.println(d);
	}

}
