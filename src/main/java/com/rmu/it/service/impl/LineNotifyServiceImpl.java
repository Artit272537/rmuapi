package com.rmu.it.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.rmu.it.service.LineNotifyService;

@Service
public class LineNotifyServiceImpl implements LineNotifyService {

	private static final Logger log = LoggerFactory.getLogger(LineNotifyServiceImpl.class);

	@Override
	public boolean sendLineNotify(String message) throws Exception {
		boolean result = false;
			if (StringUtils.isNotBlank(message)) {
				String url = "https://notify-api.line.me/api/notify";
				String TOKEN_GROUP_LINE = "eILxm6wHeKsnm6XWKdaTbzGHYI8PbbkQyl0Td1RQa4n";
				RestTemplate restTemplate = new RestTemplate();
				HttpHeaders header = new HttpHeaders();
				header.setContentType(MediaType.APPLICATION_JSON_UTF8);
				header.add("Authorization", "bearer " + TOKEN_GROUP_LINE);
				Map<String, String> map = new HashMap<>();
				map.put("message", message);
				Gson gson = new Gson();
				String body = gson.toJson(map);
				HttpEntity<String> request = new HttpEntity<>(body,header);
				log.info("request ##### " + request.toString());
				String response = restTemplate.postForObject(url, request, String.class);
				log.info("response=>" + response);
			}



		return result;
	}

}
