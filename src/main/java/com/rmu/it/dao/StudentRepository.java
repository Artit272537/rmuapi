package com.rmu.it.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.rmu.it.entity.StudentEntity;

public interface StudentRepository extends CrudRepository<StudentEntity, Long>{
	
	StudentEntity findStudentByCitizen(@Param(value = "citizen") String citizen);
	
}
